package game

import game.Countries.{Value => Country}
import game.Player.PlayerId

object Player {
    type PlayerId = Int
}

case class Player(
  id : PlayerId,
  nickname : String,
  country : Country
)