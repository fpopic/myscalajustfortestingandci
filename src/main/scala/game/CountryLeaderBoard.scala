package game

import game.Countries.{Value => Country}


case class CountryLeaderboardEntry(
    country : Country,
    points : Int
)

//noinspection AccessorLikeMethodIsEmptyParen
trait CountryLeaderboard {
    def addVictoryForCountry(country : Country) : Unit

    def getTopCountries() : List[CountryLeaderboardEntry]
}

