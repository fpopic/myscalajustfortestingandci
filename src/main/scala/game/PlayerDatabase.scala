package game

import game.Player.PlayerId

trait PlayerDatabase {
    def getPlayerById(playerId : PlayerId) : Player
}
