name := "MyScalaJustForTestingAndCI"

version := "1.0"

scalaVersion := "2.11.8"

libraryDependencies ++= Seq(
    "org.scalamock" %% "scalamock-scalatest-support" % "3.2.2" % "test",
    "org.scala-lang" % "scala-reflect" % "2.11.8",
    "org.scala-lang.modules" % "scala-xml_2.11" % "1.0.4"
)